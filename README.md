# Quick search shortcuts for Debian resources

A simple collection of bookmarks for Debian resources which hooks into
Firefox's "quick search" AKA "shortcut URL" functionality.

## Why?

After typing `security-tracker.debain.org` (notice the typo and
*don't* type it!) by hand in a web browser, a Debian developer was
served with a phishing site trying to scam information out of
him. Outraged, he sought legal assistance. While waiting for hell to
freeze over, he made an alternative solution after suggestions from
`#debian-devel` fellows on IRC.

Note that there is already a package called [debianbuttons](https://tracker.debian.org/pkg/debianbuttons) which
accomplishes a similar goal. A bookmarks file like this is more
lightweight and doesn't require a mouse to operate.

## How?

This is a [bookmarks](https://en.wikipedia.org/wiki/Bookmark_(World_Wide_Web)) file which contains a few of the most
commonly used resources in the [Debian](https://debian.org) infrastructure. Each
bookmark has a "[shortcut url](https://support.mozilla.org/en-US/kb/create-desktop-shortcut-website)", a one-letter (or sometimes a
little more) designation that can be typed in an address bar to jump
to one of those sites.

To import this in your web browser, follow the [instructions to import
a bookmarks file](https://support.mozilla.org/en-US/kb/restore-bookmarks-from-backup-or-move-them#w_using-a-bookmark-backup-file) while they still exist.

### List of shortcuts

 * `i`: [Debian ITP and RFP packages](https://wnpp.debian.net/?type%5B%5D=ITP&type%5B%5D=RFP&project=)
 * `w`: [Debian WNPP packages](https://wnpp.debian.net/)
 * `b`: [Debian Bug Tracking System (BTS)](https://bugs.debian.org/)
 * `t`: [Debian Package Tracker (PTS)](https://tracker.debian.org/)
 * `d`: [Debian Developer Overview (DDPO)](https://qa.debian.org/developer.php)
 * `l`: [Debian Mailing lists Search](https://lists.debian.org/)
 * `p`: [Debian Packages](https://packages.debian.org/)
 * `dmd`: [Debian Maintainer Dashboard (UDD)](https://udd.debian.org/dmd/)
 * `bd`: [Debian Package Auto-building (buildd)](https://buildd.debian.org/)
 * `s`: [Security Bug Tracker](https://security-tracker.debian.org/tracker/)
 * `r`: [Debian Reproducible Builds](https://tests.reproducible-builds.org/)

## Who?

The Debian developer responsible for this silliness is Antoine Beaupré
AKA `anarcat` but obviously contributions from others are welcome.

## Similar projects

This is similar in spirit or implementation to the following projects:

 * [debianbuttons](https://tracker.debian.org/pkg/debianbuttons): Firefox plugin
 * [surfraw](https://tracker.debian.org/pkg/surfraw): commandline tool
